package com.globaldriver.model

class DriverListingData {

    var countryName: String? = null
    var driverList = mutableListOf<DriverData>()
}
