package com.globaldriver.view.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.globaldriver.R

class SplashActivity : AppCompatActivity() {

    private val SPLASH_SCREEN_TIME = 1500
    private var mHandler = Handler()
    private var mRunnable: Runnable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        init()

    }

    private fun init() {

        mRunnable = Runnable {

            val intent_tour = HomeActivity.newIntent(this)
            startActivity(intent_tour)
            this@SplashActivity.finish()
        }

        mHandler.postDelayed(mRunnable, SPLASH_SCREEN_TIME.toLong())
    }

    override fun onBackPressed() {
        mHandler.removeCallbacks(mRunnable)
        super.onBackPressed()
    }
}
