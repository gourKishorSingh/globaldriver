package com.globaldriver.view.fragment

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout

import com.globaldriver.R
import com.globaldriver.model.DriverData
import com.globaldriver.model.DriverListingData
import com.globaldriver.view.adapter.CourseRVAdapter
import kotlinx.android.synthetic.main.fragment_driver_listing_layout.*

class DriverListingFragment : Fragment() {

    private val driverListingData = mutableListOf<DriverListingData>()
    private var mContext: Context?= null

    companion object {

        fun newInstance(): DriverListingFragment {
            return DriverListingFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_driver_listing_layout, container, false)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mContext = context
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        createDummyData()

        vertical_country_list.layoutManager = LinearLayoutManager(activity)
        vertical_country_list.setHasFixedSize(true)
        vertical_country_list.adapter = CourseRVAdapter(mContext, driverListingData)

    }

    fun createDummyData() {
        val driverdata = DriverData()
        driverdata.driverName = "some name"

        val driverListing = DriverListingData()
        for (i in 1..10) {
            driverListing.countryName = "India"
            val drivers = mutableListOf<DriverData>()
            for (j in 1..6) {
                drivers.add(0, driverdata)
            }
            driverListing.driverList?.addAll(drivers)
            driverListingData.add(0, driverListing)
        }
    }
}
