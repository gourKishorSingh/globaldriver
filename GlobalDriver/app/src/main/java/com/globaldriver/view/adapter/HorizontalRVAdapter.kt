package com.globaldriver.view.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.globaldriver.R
import com.globaldriver.model.DriverData

class HorizontalRVAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var mDataList: List<DriverData>? = null
    private var mRowIndex = -1

    fun setData(data: MutableList<DriverData>?) {
        if (mDataList !== data) {
            mDataList = data
            notifyDataSetChanged()
        }
    }

    fun setRowIndex(index: Int) {
        mRowIndex = index
    }

    private inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val text: TextView

        init {
            text = itemView.findViewById<View>(R.id.dl_driver_name) as TextView
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val context = parent.context
        val itemView = LayoutInflater.from(context).inflate(R.layout.item_driver_list_view, parent, false)
        return ItemViewHolder(itemView)
    }

    override fun onBindViewHolder(rawHolder: RecyclerView.ViewHolder, position: Int) {
        val holder = rawHolder as ItemViewHolder
        holder.text.text = mDataList!![position].driverName
        holder.itemView.tag = position
    }

    override fun getItemCount(): Int {
        if (mDataList == null)
            return 0;
        return 6
    }

}