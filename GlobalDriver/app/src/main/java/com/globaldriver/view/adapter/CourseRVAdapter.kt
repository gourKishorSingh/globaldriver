package com.globaldriver.view.adapter

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.globaldriver.R
import com.globaldriver.model.DriverListingData

import java.util.ArrayList

class CourseRVAdapter(private val mContext: Context?, data: MutableList<DriverListingData>?) : RecyclerView.Adapter<CourseRVAdapter.SimpleViewHolder>() {

    class SimpleViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val title: TextView
        val horizontalAdapter: HorizontalRVAdapter

        init {
            val context = itemView.context
            title = view.findViewById<View>(R.id.dl_country_name) as TextView
            horizontalList = itemView.findViewById<View>(R.id.dl_driver_list_view) as RecyclerView
            horizontalList!!.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            horizontalAdapter = HorizontalRVAdapter()
            horizontalList!!.adapter = horizontalAdapter
        }
    }

    init {
        if (data != null)
            mData = ArrayList(data)
        else
            mData = ArrayList()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimpleViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.item_driver_listing_list_view, parent, false)
        return SimpleViewHolder(view)
    }

    override fun onBindViewHolder(holder: SimpleViewHolder, position: Int) {
        holder.title.text = mData!![position].countryName
        holder.horizontalAdapter.setData(mData!![position].driverList) // List of Strings
        holder.horizontalAdapter.setRowIndex(position)
    }

    override fun getItemCount(): Int {
        return mData!!.size
    }

    companion object {
        private var mData: MutableList<DriverListingData>? = null
        private var horizontalList: RecyclerView? = null
    }

}
